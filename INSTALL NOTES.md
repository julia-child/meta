# From first setup

## 1) Install Docker

```bash
sudo yum update
sudo amazon-linux-extras install docker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo reboot
```

## 2) Install GitLab Runner

```bash
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
sudo yum install gitlab-runner
```

## 3) Register GitLab Runner

```bash
sudo gitlab-runner register
```